<?php
/**
 * @file
 * Settings form for field_group_block module.
 */

/**
 * Configuration form.
 */
function field_group_block_config_form($form, &$form_state) {
  $groups = field_group_info_groups();
  $groups_fgb = variable_get('field_group_block_groups', array());
  
  $form['groups'] = array(
    '#type' => 'fieldset',
    '#title' => t('Groups'),
    '#tree' => TRUE,
  );
  
  foreach ($groups as $entity => $bundles) {
    $form['groups'][$entity] = array(
      '#type' => 'fieldset',
      '#title' => $entity,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    foreach ($bundles as $bundle => $modes) {
      $form['groups'][$entity][$bundle] = array(
        '#type' => 'fieldset',
        '#title' => $bundle,
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      foreach ($modes as $mode => $groups_array) {
        $form['groups'][$entity][$bundle][$mode] = array(
          '#type' => 'fieldset',
          '#title' => $mode,
          '#collapsible' => TRUE,
      		'#collapsed' => TRUE,
        );
        foreach ($groups_array as $group => $group_obj) {
          $form['groups'][$entity][$bundle][$mode][$group] = array(
            '#type' => 'checkbox',
            '#title' => $group,
            '#default_value' => !empty($groups_fgb[$entity][$bundle][$mode][$group]) ? $groups_fgb[$entity][$bundle][$mode][$group] : 0,
          );
        }
      }
    }
  }
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Configuration form validate.
 */
//function field_group_block_config_form_validate() {}

/**
 * Configuration form submit.
 */
function field_group_block_config_form_submit($form, &$form_state) {
  variable_set('field_group_block_groups', $form_state['values']['groups']);
}